const Work = (function() {
    let payAmount = 0;

    return {
        getPayAmount: function() {
            return payAmount;
        },
        getWorkPayment: function() {
            payAmount += 100;
        },
        transferPayToBank: function(Bank) {
            if (Bank.getHasLoan()) {
                const loanCut = payAmount * .1;
                Bank.payTowardsLoan(loanCut);
                payAmount -= loanCut;
            }
            Bank.addBalance(payAmount);
            payAmount = 0;
        },
        transferPayToRepayLoan: function(Bank) {
            if (Bank.getHasLoan()) {
                Bank.payTowardsLoan(payAmount);
                payAmount = 0;
            }
        }
    }
})();

export default Work;