const Bank = (function() {
    let balanceAmount = 0;
    let hasLoan = false;
    let loanAmount = 0;

    return {
        getBalance: function() {
            return balanceAmount;
        },
        addBalance: function(value) {
            balanceAmount += value;
        },
        removeBalance: function(value) {
            balanceAmount -= value;
        },
        getHasLoan: function() {
            return hasLoan;
        },
        getLoanAmount: function() {
            return loanAmount;
        },
        setLoanAmount: function(value) {
            loanAmount = value; 
            hasLoan = true;
        },
        payTowardsLoan: function(value) {
            loanAmount -= value;
            if (loanAmount === 0) {
                hasLoan = false;
            } else if (loanAmount < 0) {
                const overpay = Math.abs(loanAmount);
                balanceAmount += overpay;
                loanAmount = 0;
                hasLoan = false;
            }
        }
    }
})();

export default Bank;