const Laptops = (function() {
    let laptops = [];
    let selectedLaptop;

    return {
        getLaptops() {
            return laptops;
        },
        setLaptops(arr) {
            laptops = arr;
        },
        getLaptop(index) {
            selectedLaptop = laptops[index];
            return selectedLaptop;
        },
        buySelectedLaptop(Bank) {
            if (Bank.getBalance() >= selectedLaptop.price) {
                Bank.removeBalance(selectedLaptop.price);
                alert(`Congrats! You've bought the ${selectedLaptop.title}`)
            } else {
                alert("You can't afford it! :(");
            }
        }
    }
})();

export default Laptops;