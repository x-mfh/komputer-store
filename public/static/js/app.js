import Bank from './modules/Bank.js';
import Work from './modules/Work.js';
import Laptops from './modules/Laptops.js';

// Bank elements
const elBalanceAmountSpan = document.getElementById('balance-amount');
const elLoanInfoDiv = document.getElementById('loan-info');
const elLoanAmountSpan = document.getElementById('loan-amount');
const elGetLoanBtn = document.getElementById('get-loan');
// Work Elements
const elPayAmountSpan = document.getElementById('pay-amount');
const elTransferPayToBankBtn = document.getElementById('bank-transfer');
const elGetPaymentBtn = document.getElementById('get-payment');
const elRepayLoanBtn = document.getElementById('repay-loan');
// Laptop elements
const elLaptopSelectorSelect = document.getElementById('laptop-selector');
const elLaptopFeaturesUl = document.getElementById('laptop-features');
const elLaptopShowcaseImg = document.getElementById('laptop-showcase-img');
const elLaptopShowcaseNameH2 = document.getElementById('laptop-showcase-name');
const elLaptopShowcaseInfoP = document.getElementById('laptop-showcase-info');
const elLaptopShowcasePriceP = document.getElementById('laptop-showcase-price');
const elLaptopShowcaseBuyBtn = document.getElementById('laptop-showcase-buy');

// Bank event listeners
elGetLoanBtn.addEventListener("click", handleGetLoanClick)
// Work event listeners
elTransferPayToBankBtn.addEventListener("click", handleTransferPayToBankClick)
elGetPaymentBtn.addEventListener("click", handleGetPaymentClick)
elRepayLoanBtn.addEventListener("click", handleRepayLoanClick)
// Laptop event listeners
elLaptopSelectorSelect.addEventListener("change", handleLaptopSelectChange)
elLaptopShowcaseBuyBtn.addEventListener("click", handleLaptopBuyBtnClick)

// initializers
loadBankInfo();
loadLaptopsAsync();

function loadBankInfo() {
    elBalanceAmountSpan.innerText = `${Bank.getBalance()} kr`;
    if (Bank.getHasLoan()) {
        elLoanInfoDiv.style.display = "block";
        elRepayLoanBtn.style.display = "inline-block";
        elLoanAmountSpan.innerText = `-${Bank.getLoanAmount()} kr`;
        elGetLoanBtn.disabled = true;
    } else {
        elLoanInfoDiv.style.display = "none";
        elRepayLoanBtn.style.display = "none";
        elGetLoanBtn.disabled = false;
    }
}

async function loadLaptopsAsync() {
    try {
        const res = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
        const fetchedLaptops = await res.json();
        Laptops.setLaptops(fetchedLaptops);
        Laptops.getLaptops().forEach(addLaptopToSelect);
        loadLaptopInfo(Laptops.getLaptop(0));
    } catch (error) {
        console.error('FAILED TO FETCH LAPTOPS')
        console.error('ERROR: ', error);
    }
}

function addLaptopToSelect(laptop) {
    const elLaptopOption = document.createElement('option');
    elLaptopOption.value = laptop.id;
    elLaptopOption.appendChild(document.createTextNode(laptop.title));
    elLaptopSelectorSelect.appendChild(elLaptopOption);
}

function loadLaptopInfo(laptop) {
    loadLaptopFeatures(laptop);
    loadLaptopShowcase(laptop);
}

function loadLaptopFeatures(laptop) {
    elLaptopFeaturesUl.innerHTML = '';
    laptop.specs.forEach(spec => {
        const elLaptopSpecLi = document.createElement('li');
        elLaptopSpecLi.textContent = spec;
        elLaptopFeaturesUl.appendChild(elLaptopSpecLi);
    });
}

async function loadLaptopShowcase(laptop) {
    elLaptopShowcaseNameH2.textContent = laptop.title;
    elLaptopShowcaseInfoP.textContent = laptop.description;
    elLaptopShowcasePriceP.textContent = `${laptop.price} DKK`;
    elLaptopShowcaseImg.src = 'https://noroff-komputer-store-api.herokuapp.com/' + laptop.image
}

function handleGetLoanClick() {
    const loanAmountRequest = prompt('How much would you like to loan?');
    if (loanAmountRequest === null) return;
    const loanAmountParsed = parseInt(loanAmountRequest)
    if(!isNaN(loanAmountParsed) && loanAmountParsed > 0) {
        if (loanAmountParsed > Bank.getBalance() * 2) {
            alert("You can't loan more than double of your balance");
        } else {
            Bank.setLoanAmount(loanAmountParsed);
            Bank.addBalance(loanAmountParsed);
            loadBankInfo();
            alert(`You've loaned ${loanAmountParsed} kr`);
        }
    } else {
        alert('Enter a number above 0');
    }
}

function handleTransferPayToBankClick() {
    Work.transferPayToBank(Bank);
    elPayAmountSpan.innerText = `${Work.getPayAmount()} kr`;
    loadBankInfo();
}

function handleGetPaymentClick() {
    Work.getWorkPayment();
    elPayAmountSpan.innerText = `${Work.getPayAmount()} kr`;
}

function handleRepayLoanClick() {
    Work.transferPayToRepayLoan(Bank);
    elPayAmountSpan.innerText = `${Work.getPayAmount()} kr`;
    loadBankInfo();
}

function handleLaptopSelectChange(e) {
    const selectedLaptop = Laptops.getLaptop(e.target.selectedIndex);
    loadLaptopInfo(selectedLaptop);
}

function handleLaptopBuyBtnClick() {
    Laptops.buySelectedLaptop(Bank);
    loadBankInfo();
}